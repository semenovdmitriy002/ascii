﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASCII
{
    internal class Program
    {
        private const double V = 1.5;

        [STAThread]
        static void Main(string[] args)
        {


            var openFileDialog = new OpenFileDialog()
            {
                Filter = "Images | *.bmp; *.png; *.jpg; *.JPEG"
            };

            Console.WriteLine("Press enter to start...\n");

            while (true)
            {
                Console.ReadLine();

                if (openFileDialog.ShowDialog() != DialogResult.OK)
                    continue;

                Console.Clear();

                var bitmap = new Bitmap(openFileDialog.FileName);
                bitmap = ResizeBitmap(bitmap);
                bitmap.ToGrayScale();

                var conaverter = new BitmapToASCIIConverter(bitmap);
                var rows = conaverter.Convert();

                foreach (var row in rows)
                {
                    Console.WriteLine(row);
                }
                Console.SetCursorPosition(0, 0);

            }
        }

        private static Bitmap ResizeBitmap(Bitmap bitmap)
        {
            var maxWidth = 350;
            var newHeight = bitmap.Height / V * maxWidth / bitmap.Width;
            if(bitmap.Width> maxWidth||bitmap.Height> newHeight)
            {
                bitmap = new Bitmap(bitmap, new Size(maxWidth,(int) newHeight));
            }
            return bitmap;
        }
    }
}
